function term_kpsa

global instance_name;
global initial_solution ;
global out_sol_filename;
global obj_value;
global ntime;
global obj_value_sol;

command=['java -jar ttplab-bbox.jar kpsa ' instance_name ' ' initial_solution ' ' out_sol_filename];
[status,result]=dos(command);               
teste=initial_solution;
arquivo = fopen(teste);
input = fscanf(arquivo,'%c');
fclose(arquivo);
teste=out_sol_filename; %precisa ler o output sem a primeira linha
arquivo = fopen(teste);
output = fscanf(arquivo,'%c');
fclose(arquivo);
[b solution]=strtok(output,'_');
[header b]=strtok(input,'_');
space=find(result==' ');
obj_value=str2double(result(space(1)+1:space(2)-1));
 %fprintf('obj_value kpsa=%f. \n',obj_value);
 obj_value_sol= [obj_value_sol; obj_value];
input=strcat(header,solution);
dlmwrite(initial_solution,input,'delimiter','');%write input in txt file

ntime=ntime+1;
