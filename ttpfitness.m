function ind = ttpfitness(ind,params,data,terminals,varsvals)
%TTPFITNESS   Measures the fitness of a GPLAB HHTTP INDIVIDUAL (tree).
%   TTPFITNESS(INDIVIDUAL,PARAMS,DATA,TERMINALS,VARSVALS) returns
%   the fitness of INDIVIDUAL, measured as the output from a heuritics 
%   sequence (tree str) applied to TTP problem during 'maxtime' time steps. 
%   Also returns other variables as global variables.
%
%   Input arguments:
%      INDIVIDUAL - the individual whose fitness is to measure (struct)
%      PARAMS - the current running parameters (struct)
%      DATA - the dataset on which to measure the fitness (struct)
%      TERMINALS - (not needed here - kept for compatibility purposes)
%      VARSVALS - (not needed here - kept for compatibility purposes)
%   Output arguments:
%      INDIVIDUAL - the individual whose fitness was measured (struct)
%
%fitness is the sum of normalize fitness of each instances
% param Lowisbetter must be 0


global obj_value_sol;
global obj_value;
global maxtime;
global instance_name;
global instances;
global initial_solution; 
global optimum; 

obj_value=0;
maxtime=3;
obj_value_instance=[];

n=size(instances,1);
% (repeat program until maxtime)
for i=1:n
    instance_name=char(instances(i));

    command=['java -jar ttplab-bbox.jar initlk ' instance_name ' none ' initial_solution];
    [status res]=dos(command); 

    ttpeval(ind.tree);

    if obj_value>optimum(i)
        optimum(i)=obj_value; %maximum objective value until now for instance i
    end        
    %fprintf('optimum %f =%f\n',i,optimum(i));
    %fprintf('instance %d \n',i);
    
    obj_value_instance=[obj_value_instance; obj_value];
end

%  fprintf('Individuo');
%  title('Individuo');
%         drawtree(ind.tree) 
%fitness must be mean of all instances - normalized 
%fprintf('obj_value_instance=%f\n',obj_value_instance);
%obj_value=sum(obj_value_instance);
maxi=max(obj_value_instance);
mini=min(obj_value_instance);
if maxi==0 || maxi-mini==0
    obj_value_norm=obj_value_instance; 
else
    obj_value_norm=obj_value_instance/(maxi-mini);%- normalized
end
obj_value=sum(obj_value_norm); %param lowisbetter must be 0
fprintf('obj_value=%f\n',obj_value);


% raw fitness:
ind.fitness=obj_value; %higher fitness means better individual
%fprintf('individuo.\n');
%initial_solution='instance_input.txt'; %restore the original input file
obj_value_sol=[]; %reset terminal objective values for the next individual
ind.result(1)=ind.fitness;
