%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This scrit calculates the A-measure according A-test. 
% This test tells us how often, on average, one technique outperforms the 
% other. Its a non-parametric test called the measure of stochastic
% superiority. The hypothesis of stochastic equality ($A = 0.5$) can
% be tested by using any of several methods, and the most related is the 
% Mann-Whitney-Wilcoxon rank, which was used and have been executed with a 
% confidence level of 95%.
% A measure is a value between 0 and 1: when the A measure is exactly 0.5,
% then the two techniques achieve equal performance;
% when A is less than 0.5, the first technique is worse;
% and when A is more than 0.5, the second technique is worse.
% The closer to 0.5, the smaller the difference between the techniques;
% the farther from 0.5, the larger the difference.
% References:
% http://markread.info/2014/04/free-statistics-code/
% http://doofussoftware.blogspot.com.br/2012/07/measuring-effect-siz
% e-with-vargha.html

% This test is a pairwise comparison.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        


        %algoritms results for comparison
        fname1='hsgp'; %file names as cvs files
        fname2='s5';

        fileID = fopen(strcat('output/',fname1,'.csv'),'r');
        algo1 = fscanf(fileID,'%c');
        fclose(fileID);
        algo1 = strread(algo1,'%s');
        instance_name=algo1(1:3:size(algo1,1));
        algo1=str2double(algo1);  
        algo1=algo1(2:3:size(algo1,1)); %just the objective values

        fileID = fopen(strcat('output/',fname2,'.csv'),'r');
        algo2 = fscanf(fileID,'%c');
        fclose(fileID);
        algo2 = strread(algo2,'%s');
        algo2=str2double(algo2);  
        algo2=algo2(2:3:size(algo2,1)); %just the objective values


        t=10;%number of runs for each instance
        n=size(algo1,1)/10; %27*4;%number of instances (eil51, berlin52, eil76, kroA100)

        %instance names
        instance_name=instance_name(1:t:size(instance_name,1));

        %% Atest
        for i=1:n
            if (i==1)
                x=algo1(1:1*t);
                y=algo2(1:1*t);
                p(i) = Atest(x,y); 
            end
                if (i==2)
                x=algo1(t+1:i*t);
                y=algo2(t+1:i*t);
                p(i) = Atest(x,y);
                else
                x=algo1((i-1)*t+1:i*t);
                y=algo2((i-1)*t+1:i*t);
                p(i) = Atest(x,y);
                end
                %A=p'
                if p(i)>0.5
                    res_test=strcat(fname1);%, fname1 ' is better than', fname2);
                else
                    if p(i)==0.5
                     res_test='equal';%, fname1 ' has equal performance as', fname2);   
                    else
                        res_test=strcat(fname2);%, fname1 ' is worse than', fname2);
                    end

                end
%                 fileID = fopen(strcat('output/Atest',fname1,'x',fname2,'.csv'),'a');
%                 fprintf(fileID,'%s, %d, %s\n', char(instance_name(i)), p(i),res_test);
%                 fclose(fileID);
        end
        p=p'; 
        


