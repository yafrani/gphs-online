function term_kpbf

global instance_name;
global initial_solution ;
global out_sol_filename;
global obj_value;
global ntime;
global obj_value_sol;

command=['java -jar ttplab-bbox.jar kpbf ' instance_name ' ' initial_solution ' ' out_sol_filename];
[status,result]=dos(command);

%reading input.txt 
teste=initial_solution;
arquivo = fopen(teste);
input = fscanf(arquivo,'%c');
fclose(arquivo);

%reading output.txt 
teste=out_sol_filename; 
arquivo = fopen(teste);
output = fscanf(arquivo,'%c');
fclose(arquivo);

%output file must be readen without first line
[b solution]=strtok(output,'_');
[header b]=strtok(input,'_');

%find the objective value - read 'space' character in string until find
%objective value
space=find(result==' ');
obj_value=str2double(result(space(1)+1:space(2)-1));
%fprintf('obj_value kpbf=%f. \n',obj_value);
obj_value_sol= [obj_value_sol; obj_value];

%new input file will be the current output
input=strcat(header,solution);
dlmwrite(initial_solution,input,'delimiter','');%write input in txt file

ntime=ntime+1;
