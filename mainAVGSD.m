% Average + standard deviation

%algoritms results for comparison
fname1='hsgp'; %filename
fname2='ma2b';
fname3='cs2sa';
fname4='s5';
%ma2b, cs2sa, s5, and hyperGP

fileID = fopen(strcat('output/',fname1,'.csv'),'r');
algo1 = fscanf(fileID,'%c');
fclose(fileID);
algo1 = strread(algo1,'%s');
instance_name=algo1(1:3:size(algo1,1));
algo1=str2double(algo1);  
algo1=algo1(2:3:size(algo1,1)); %just the objective values

fileID = fopen(strcat('output/',fname2,'.csv'),'r');
algo2 = fscanf(fileID,'%c');
fclose(fileID);
algo2 = strread(algo2,'%s');
algo2=str2double(algo2);  
algo2=algo2(2:3:size(algo2,1)); %just the objective values

fileID = fopen(strcat('output/',fname3,'.csv'),'r');
algo3 = fscanf(fileID,'%c');
fclose(fileID);
algo3 = strread(algo3,'%s');
algo3=str2double(algo3);  
algo3=algo3(2:3:size(algo3,1)); %just the objective values

fileID = fopen(strcat('output/',fname4,'.csv'),'r');
algo4 = fscanf(fileID,'%c');
fclose(fileID);
algo4 = strread(algo4,'%s');
algo4=round(str2double(algo4));  
algo4=algo4(2:3:size(algo4,1)); %just the objective values


nalg=4; %number of algorithms to be compared
t=10;%number of runs for each instance
n=size(algo1,1)/10; %27*4;%number of instances (eil51, berlin52, eil76, kroA100)

%instance names
instance_name=instance_name(1:t:size(instance_name,1));

%% This algo matrix must be adjustable according the number of algorithms
%% will be compared
for i=1:n    
     if (i==1)
        algo(:,i)=[algo1(1:1*t);algo2(1:1*t);algo3(1:1*t);algo4(1:1*t)]; %matrix where rows are algorithms results (10 rows each algo) and columns are instances
        avg(1,i)=mean(algo1(1:t));
        dev(1,i)=std(algo1(1:t));
        avg(2,i)=mean(algo2(1:t));
        dev(2,i)=std(algo2(1:t));
        avg(3,i)=mean(algo3(1:t));
        dev(3,i)=std(algo3(1:t));
        avg(4,i)=mean(algo4(1:t));
        dev(4,i)=std(algo4(1:t));
     end
        if (i==2)
        algo(:,i)=[algo1(t+1:i*t);algo2(t+1:i*t);algo3(t+1:i*t);algo4(t+1:i*t)]; 
        avg(1,i)=mean(algo1(t+1:i*t));
        dev(1,i)=std(algo1(t+1:i*t));
        avg(2,i)=mean(algo2(t+1:i*t));
        dev(2,i)=std(algo2(t+1:i*t));
        avg(3,i)=mean(algo3(t+1:i*t));
        dev(3,i)=std(algo3(t+1:i*t));
        avg(4,i)=mean(algo4(t+1:i*t));
        dev(4,i)=std(algo4(t+1:i*t));
        else
        algo(:,i)=[algo1((i-1)*t+1:i*t);algo2((i-1)*t+1:i*t);algo3((i-1)*t+1:i*t);algo4((i-1)*t+1:i*t)]; 
        avg(1,i)=mean(algo1((i-1)*t+1:i*t));
        dev(1,i)=std(algo1((i-1)*t+1:i*t));     
        avg(2,i)=mean(algo2((i-1)*t+1:i*t));
        dev(2,i)=std(algo2((i-1)*t+1:i*t));
        avg(3,i)=mean(algo3((i-1)*t+1:i*t));
        dev(3,i)=std(algo3((i-1)*t+1:i*t));
        avg(4,i)=mean(algo4((i-1)*t+1:i*t));
        dev(4,i)=std(algo4((i-1)*t+1:i*t));
        end
end
       %boxplot([algo1(1:t),algo2(1:t),algo3(1:t),algo4(1:t)],'Notch','on',
       %'Labels',{'hyperGP','ma2b','cs2sa','s5'})% to one instance
       
% %% Separate per instance       
%        int=27;
%        n=27
%        avg=avg(:,1:int); %eil51
%        dev=dev(:,1:int); %eil51
%        
%        avg=avg(:,int+1:int*2); %berlin52
%        dev=dev(:,int+1:int*2); %berlin52
%        
%        avg=avg(:,int*2+1:int*3); %eil76
%        dev=dev(:,int*2+1:int*3); %eil76
%        
%        avg=avg(:,int*3+1:int*4); %kroA100
%        dev=dev(:,int*3+1:int*4); %kroA100
       

%% trend line with polynomial fit
%Average
poly1=polyfit(1:n,avg(1,:),6);
poly2=polyfit(1:n,avg(2,:),6);
poly3=polyfit(1:n,avg(3,:),6);
poly4=polyfit(1:n,avg(4,:),6);
X1= 1:0.1:n; % X data range 
Y1=polyval(poly1,X1);
X2= 1:0.1:n; % X data range 
Y2=polyval(poly2,X2);
X3= 1:0.1:n; % X data range 
Y3=polyval(poly3,X3);
X4= 1:0.1:n; % X data range 
Y4=polyval(poly4,X4);

%standard deviation
poly1=polyfit(1:n,dev(1,:),8);
poly2=polyfit(1:n,dev(2,:),8);
poly3=polyfit(1:n,dev(3,:),8);
poly4=polyfit(1:n,dev(4,:),8);
X1= 1:0.1:n; % X data range 
W1=polyval(poly1,X1);
X2= 1:0.1:n; % X data range 
W2=polyval(poly2,X2);
X3= 1:0.1:n; % X data range 
W3=polyval(poly3,X3);
X4= 1:0.1:n; % X data range 
W4=polyval(poly4,X4);

%% Finding the matrix avg+-sd
ic1=[];
ic2=[];
ic3=[];
ic4=[];
for i=1:size(X1,2)
ic1=[ic1;Y1(i) Y1(i)+W1(i)  Y1(i)-W1(i)];
ic2=[ic2;Y2(i) Y2(i)+W2(i)  Y2(i)-W2(i)];
ic3=[ic3;Y3(i) Y3(i)+W3(i)  Y3(i)-W3(i)];
ic4=[ic4;Y4(i) Y4(i)+W4(i)  Y4(i)-W4(i)];
end

% for i=1:X1
% ic1=[ic1;Y1(i)-W1(i) Y1(i) Y1(i)+W1(i)];
% ic2=[ic2;avg(2,i)-dev(2,i) avg(2,i) avg(2,i)+dev(2,i)]
% ic3=[ic3;avg(3,i)-dev(3,i) avg(3,i) avg(3,i)+dev(3,i)]
% ic4=[ic4;avg(4,i)-dev(4,i) avg(4,i) avg(4,i)+dev(4,i)]
% end
%% Plot confidence interval
   % Plot 2 graphs in one figure:
   %    a red line with red patch of 10% transparency and
   %    a green line with green patch of 10% transparency
   % This example shows that 2 separate graphs can be drawn
   % by by holding an axes
   y1 = ic1; %algo1
   x = X1;
   figure;
   plot_ci(x,y1, 'PatchColor', 'b', 'PatchAlpha', 0.1, ...
          'MainLineWidth', 2, 'MainLineColor', 'b', ...
          'LineWidth', 1, 'LineStyle','--', 'LineColor', 'k');
      
      
   hold on
   grid on;
   
   y2 = ic2; %algo2
   plot_ci(x,y2, 'PatchColor', 'k', 'PatchAlpha', 0.1, ...
          'MainLineWidth', 2, 'MainLineColor', 'k', ...
          'LineWidth', 1, 'LineStyle','--', 'LineColor', 'k');

   hold on
   grid on;
   
   y3 = ic3; %algo2
   plot_ci(x,y3, 'PatchColor', 'r', 'PatchAlpha', 0.1, ...
          'MainLineWidth', 2, 'MainLineColor', 'r', ...
          'LineWidth', 1, 'LineStyle','--', 'LineColor', 'k');
      hold on
   grid on;

   y4 = ic4; %algo2
   plot_ci(x,y4, 'PatchColor', 'g', 'PatchAlpha', 0.1, ...
          'MainLineWidth', 2, 'MainLineColor', 'g', ...
          'LineWidth', 1, 'LineStyle','--', 'LineColor', 'g');    
       
        title('IC');
    xlabel('number of cities');
    ylabel('ic');
    legend('HSGP','MA2B','CS2SA','S5','Location','SouthEast')
 
 %% Write the average +sd   
for i=1:n
    fileID = fopen('output/average.csv','a');
    fprintf(fileID,'%s, %d,%d, %d,%d, %d,%d, %d,%d \n', char(instance_name(i)), avg(1,i),dev(1,i),avg(2,i),dev(2,i),avg(3,i),dev(3,i),avg(4,i),dev(4,i)); 
    fclose(fileID);
end


