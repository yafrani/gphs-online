% Hyper heuristic for TTP_problem using GPLAB toolbox.
% training for eil51*, kroA100*, and a280* 
% with C={1,5,10} and F={1,5,10}

clc
gen = 500;
popsize = 10;

addpath(genpath('./gplab/'));

global instance_name;
global initial_solution;
global out_sol_filename;

%% set GP params

initial_solution = 'sol_input.txt';
out_sol_filename = 'sol_output.txt';

p = resetparams;
p.sampling = 'lexictour';%'lexictour':this method chooses tournamentsize parents by randomly  and selecting only the best of them.
p.elitism = 'keepbest';
p.survival = 'fixedpopsize';
p.initpoptype = 'fullinit';%fullinit garantees the initial minimum depth
p = setoperators(p,'crossover',2,2,'mutation',1,1);%p, paramname, num parents, num childs
p = setfunctions(p,'prog2',2);
p = setterminals(p, ...
    'term_kpbf', 'term_kpsa', ...
    'term_tsp2opt', ...
    'term_otspswap', 'term_otsp4opt', ...
    'term_okpbf20','term_okpbf30','term_okpbf40' ...
);
p.calcfitness = 'ttpfitness_online'; %fitness function
p.lowerisbetter = 0; %0: maximization 1:minimization
p.autovars = 0;
% Added after Sara anwer about single nodes
p.depthnodes = '1';% To activate strictdeph, this filter depthnodes=1, dynamiclevel=1/0, fixedlevel=1(default) manual GPLAB pg26 
p.fixedlevel = '1';%When on, the strict maximum depth of trees is determined by the parameter realmaxlevel
p.dynamiclevel = '0';%When on, its initial value is determined by the parameter inicdynlevel=8
%p.inicmaxlevel=30;%can be set if fixedlevel=0
p.realmaxlevel=7;%as the inicmaxlevel
p.realminlevel=2;%minimum depth. To activate strictdeph, this filter depthnodes=1, dynamiclevel=1/0, fixedlevel=1(default) manual GPLAB pg26 
%in the initialization process, depthnodes=1 provides 6 levels automatically for inicmaxlevel, so we must
%update it the code. So I changed checkvarparams to 4 levels
p.tournamentsize = 3;%selecting the best of whole population.
%p.initialfixedprobs=[0.9,0.1];%crossover rate, mutation rate - default [0.5,0.5]

%% run for one instance

fprintf('Running online HyperGP for one TTP problem...\n');

instance_name = 'eil51_n50_bounded-strongly-corr_01.ttp';

[v,b] = gplabHH(gen,popsize,p);

%% run many insatances

fprintf('Running online HyperGP for a subset of TTP DB...\n');

% read instances
file = 'instances_online.txt';
arquivo = fopen(file);
instances = fscanf(arquivo,'%c');
fclose(arquivo);
instances = strread(instances,'%s');

% nb of instances
nb_inst = size(instances,1);

% nb of repetitions
nb_rep = 10;

% loop & run all instances
for i = 1:nb_inst
    for j = 1:nb_rep
        % current instance
        instance_name = instances{i};
        display(['===>' instance_name]);
        
        % run for instance_name
        [v,b] = gplabHH(gen,popsize,p);
        
        % save output to file
        fileID = fopen('output/results-10-12.csv','a');
        fprintf(fileID, '%s %d %d\n', instance_name, b.fitness, b.level);
        fclose(fileID);
    end
end

%% resulting model

% draw tree
drawtree(v.state.bestsofar.tree);
% print executable string
b.str
