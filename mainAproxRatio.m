
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%This quality indicator measures the approximation ration of an algorithm.
% We consider the best objective value found over all
% algorithms runs and we take the average of the $10$ results produced by 
% an algorithm. The ratio between the average and the best objective value 
% found gives us the approximation ratio.
%References:
% Stealing items more efficiently with ants: a swarm intelligence approach 
%to the travelling thief problem (extended version) - Falkner,2015
%http://cs.adelaide.edu.au/~optlog/research/ttp/2016ants-extended.pdf

%This script works for 4 algoritms. It must be improved for more.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%algoritms results for comparison as rows in cvs files
fname1='hsgp'; %name of file
fname2='ma2b';
fname3='cs2sa';
fname4='s5';
%ma2b, cs2sa, s5, and hyperGP

fileID = fopen(strcat('output/',fname1,'.csv'),'r');
algo1 = fscanf(fileID,'%c');
fclose(fileID);
algo1 = strread(algo1,'%s');
instance_name=algo1(1:3:size(algo1,1));
algo1=str2double(algo1);  
algo1=algo1(2:3:size(algo1,1)); %just the objective values

fileID = fopen(strcat('output/',fname2,'.csv'),'r');
algo2 = fscanf(fileID,'%c');
fclose(fileID);
algo2 = strread(algo2,'%s');
algo2=str2double(algo2);  
algo2=algo2(2:3:size(algo2,1)); %just the objective values

fileID = fopen(strcat('output/',fname3,'.csv'),'r');
algo3 = fscanf(fileID,'%c');
fclose(fileID);
algo3 = strread(algo3,'%s');
algo3=str2double(algo3);  
algo3=algo3(2:3:size(algo3,1)); %just the objective values

fileID = fopen(strcat('output/',fname4,'.csv'),'r');
algo4 = fscanf(fileID,'%c');
fclose(fileID);
algo4 = strread(algo4,'%s');
algo4=round(str2double(algo4));  
algo4=algo4(2:3:size(algo4,1)); %just the objective values


nalg=4; %number of algorithms to be compared
t=10;%number of runs for each instance
n=size(algo1,1)/10; %27*4;%number of instances (eil51, berlin52, eil76, kroA100)

%instance names
instance_name=instance_name(1:t:size(instance_name,1));

%% This algo and avg matrix must be adapted according the number of algorithms
%% thet will be compared
for i=1:n    
     if (i==1)
        algo(:,i)=[algo1(1:1*t);algo2(1:1*t);algo3(1:1*t);algo4(1:1*t)]; %matrix where rows are algorithms results (10 rows each algo) and columns are instances
        avg(1,i)=mean(algo1(1:t));
        avg(2,i)=mean(algo2(1:t));
        avg(3,i)=mean(algo3(1:t));
        avg(4,i)=mean(algo4(1:t));
        
        dev(1,i)=std(algo1(1:t));
        dev(2,i)=std(algo2(1:t));
        dev(3,i)=std(algo3(1:t));
        dev(4,i)=std(algo4(1:t));
     end
        if (i==2)
        algo(:,i)=[algo1(t+1:i*t);algo2(t+1:i*t);algo3(t+1:i*t);algo4(t+1:i*t)]; 
        avg(1,i)=mean(algo1(t+1:i*t));
        avg(2,i)=mean(algo2(t+1:i*t));
        avg(3,i)=mean(algo3(t+1:i*t));
        avg(4,i)=mean(algo4(t+1:i*t));
        
        dev(1,i)=std(algo1(t+1:i*t));
        dev(2,i)=std(algo2(t+1:i*t));
        dev(3,i)=std(algo3(t+1:i*t));
        dev(4,i)=std(algo4(t+1:i*t));
        else
        algo(:,i)=[algo1((i-1)*t+1:i*t);algo2((i-1)*t+1:i*t);algo3((i-1)*t+1:i*t);algo4((i-1)*t+1:i*t)]; 
        avg(1,i)=mean(algo1((i-1)*t+1:i*t));
        avg(2,i)=mean(algo2((i-1)*t+1:i*t));
        avg(3,i)=mean(algo3((i-1)*t+1:i*t));
        avg(4,i)=mean(algo4((i-1)*t+1:i*t));
        
        dev(1,i)=std(algo1((i-1)*t+1:i*t));
        dev(2,i)=std(algo2((i-1)*t+1:i*t));
        dev(3,i)=std(algo3((i-1)*t+1:i*t));
        dev(4,i)=std(algo4((i-1)*t+1:i*t));
        end
end
    best=max(algo);%best of all algorithms for each instance
   
    aprox_ratio(1,:)=avg(1,:)./best;
    aprox_ratio(2,:)=avg(2,:)./best;
    aprox_ratio(3,:)=avg(3,:)./best;
    aprox_ratio(4,:)=avg(4,:)./best;
    
     aprox_ratio_sd(1,:)=dev(1,:)./best;
    aprox_ratio_sd(2,:)=dev(2,:)./best;
    aprox_ratio_sd(3,:)=dev(3,:)./best;
    aprox_ratio_sd(4,:)=dev(4,:)./best;
    
    
  %  plot(1:n,aprox_ratio(1,:),'b',1:n,aprox_ratio(2,:),'r')
  
  %% trend line with polynomial fit
poly1=polyfit(1:n,aprox_ratio(1,:),6);
poly2=polyfit(1:n,aprox_ratio(2,:),6);
poly3=polyfit(1:n,aprox_ratio(3,:),6);
poly4=polyfit(1:n,aprox_ratio(4,:),6);
X1= 1:0.1:n; % X data range 
Y1=polyval(poly1,X1);
X2= 1:0.1:n; % X data range 
Y2=polyval(poly2,X2);
X3= 1:0.1:n; % X data range 
Y3=polyval(poly3,X3);
X4= 1:0.1:n; % X data range 
Y4=polyval(poly4,X4);

%% Plot number of cities at x axis
% plot(X1,Y1,'b', X2,Y2,'k',X3,Y3,'r',X4,Y4,'g');
% xlabel('number of cities');
%     ylabel('average approximation ratio');
%     legend('GPHS','MA2B','CS2SA','S5','Location','SouthEast')
%     title('Performance');
%  Xt = [51 76 100];
%     Xl = [51 n];
%     set(gca,'XTick',Xt,'XLim',Xl);
    
    %%%%%%or:
plot(X1,Y1,'b', X2,Y2,'k',X3,Y3,'r',X4,Y4,'g');
xlabel('instances');
    ylabel('average approximation ratio');
    legend('GPHS','MA2B','CS2SA','S5','Location','SouthEast')
    title('Performance');
%   Xt = [51 100]; % Xt = [51 76 100];%Xt = [51 100 280 439 783];
%      Xl = [51 n];
Yl=[0.6 1]
%Xl=[0 n-26]
Xl=[0 n]
   Xt =[0 28 55 82 109 136 163]
    Xlb={'eil51','berlin52','eil76','kroA100','a280','pr439',' '}
     %Xlb={'eil51','berlin52','eil76','kroA100','a280','pr439'}
    %set(gca,'XTick',Xt,'XLim',Xl);
    set(gca,'XTick',Xt,'XTicklabel',Xlb,'XLim',Xl, 'YLim',Yl);


    %% Plot instance names at x axis
    plot(X1,Y1,'b', X2,Y2,'k',X3,Y3,'r',X4,Y4,'g');
    % Reduce the size of the axis so that all the labels fit in the figure.
    pos = get(gca,'Position');
    set(gca,'Position',[pos(1), .4, pos(3) .5])

    title('Performance');

    % Set the X-Tick locations so that every other inatsnce is labeled.
    Xt = 1:27:n;
    Xl = [1 n];
    set(gca,'XTick',Xt,'XLim',Xl);
    
    % % same for Y-Tick
    Yt = 0.6:.1:1.02;
    Yl = [0.6 1.02];
    set(gca,'YTick',Yt,'YLim',Yl);

    % ax = axis;    % Current axis limits
    % axis(axis);    % Set the axis limit modes (e.g. XLimMode) to manual
    % Yl = ax(3:4);  % Y-axis limits
    
    % Place the text labels
    t = text(Xt,Yl(1)*ones(1,length(Xt)),instance_name(1:27:n,:));
    set(t,'HorizontalAlignment','right','VerticalAlignment','top', ...
          'Rotation',60,'FontSize',8);

    % Remove the default labels
    set(gca,'XTickLabel','')
    % Get the Extent of each text object.  This
    % loop is unavoidable.
    for i = 1:length(t)
      ext(i,:) = get(t(i),'Extent');
    end
    % Determine the lowest point.  The X-label will be
    % placed so that the top is aligned with this point.
    LowYPoint = min(ext(:,2));
    % Place the axis label at this point
    XMidPoint = Xl(1)+abs(diff(Xl))/2;
    tl = text(XMidPoint,LowYPoint,'', ...
              'VerticalAlignment','top', ...
              'HorizontalAlignment','center');  
       % xlabel('number of cities');
        ylabel('average approximation ratio');
        legend('GPHS','MA2B','CS2SA','S5','Location','SouthEast')
    
 
 %% Write the approximation ratio    
for i=1:n
    fileID = fopen('output/aprox_ratio.csv','a');
    fprintf(fileID,'%s, %d, %d, %d, %d \n', char(instance_name(i)), aprox_ratio(1,i),aprox_ratio(2,i),aprox_ratio(3,i),aprox_ratio(4,i));
    fclose(fileID);
end


