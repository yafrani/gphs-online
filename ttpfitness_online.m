function ind = ttpfitness_online(ind,params,data,terminals,varsvals)
%   TTPFITNESS Measures the fitness of a GPLAB HHTTP INDIVIDUAL (tree).
%   TTPFITNESS(INDIVIDUAL,PARAMS,DATA,TERMINALS,VARSVALS) returns
%   the fitness of INDIVIDUAL, measured as the output from a heuritics 
%   sequence (tree str) applied to TTP problem during 'maxtime' time steps. 
%   Also returns other variables as global variables.
%
%   Input arguments:
%      INDIVIDUAL - the individual whose fitness is to measure (struct)
%      PARAMS - the current running parameters (struct)
%      DATA - the dataset on which to measure the fitness (struct)
%      TERMINALS - (not needed here - kept for compatibility purposes)
%      VARSVALS - (not needed here - kept for compatibility purposes)
%   Output arguments:
%      INDIVIDUAL - the individual whose fitness was measured (struct)

global obj_value;
global initial_solution; 
global instance_name;
global e;
global obj_value_sol;

obj_value=0;
obj_value_sol=[];

command=['java -jar ttplab-bbox.jar initlkg ' instance_name ' none ' initial_solution];
[status res]=dos(command); 
% % 
% %% In windows case, we already have the initial solution
%         sol_init = strcat('initial-solutions/',instance_name,'_sol_input.txt');
%         %sol_init = strcat('initial-solutions/',instance_name,'_sol_input_',int2str(e),'.txt');
%         arquivo = fopen(sol_init);
%         instance_init_solution = fscanf(arquivo,'%c');
%         fclose(arquivo);
%         dlmwrite(initial_solution,instance_init_solution,'delimiter','');%re-write the initial solution
%         %fprintf('Initial solutin found!\n');
% %%


ttpeval(ind.tree);

%fprintf('obj_value=%f\n',obj_value);


% raw fitness:
ind.fitness = obj_value; %higher fitness means better individual
ind.result(1) = ind.fitness;
%fprintf('ind.fitness=%f\n',ind.fitness);
