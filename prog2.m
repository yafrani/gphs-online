function prog2(actions)
%prog2    Executes two actions on GP TTP_problem.
%  prog2 executes ACTIONS{1} followed by ACTIONS{2}. Returns the
%   number of the time step after both actions.
%
%   Input arguments:
%      ACTIONS - two actions to be executed sequentially (cell array)
%

ttpeval(actions{1});
ttpeval(actions{2});
